<?php

require('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo "Name : $sheep->name <br>"; // "shaun"
echo "legs : $sheep->legs<br>"; // 4
echo "cold blooded : $sheep->cold_blooded<br><br>"; // "no"


$kodok = new Frog ("Buduk");

echo "Name : $kodok->name <br>"; // "Buduk"
echo "legs : $kodok->legs<br>"; // 4
echo "cold blooded : $kodok->cold_blooded<br>"; // "no"
echo "Jump : ";$kodok->jump();
echo "<br><br>";


$Sungokong = new Ape ("Kera Sakti");

echo "Name : $Sungokong->name <br>"; // "kera"
echo "legs : $Sungokong->legs<br>"; // 2
echo "cold blooded : $Sungokong->cold_blooded<br>"; // "no"
echo "Yell : ";$Sungokong->yell();
echo "<br><br>";

